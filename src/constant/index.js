export const CATEGORIES = {
  VEGETABLES: 0,
  TUBER: 1,
  FRUIT: 2
};

export const PRODUCT_STATUS = {
  SELLING: 0,
  ARCHIVED: 1
};