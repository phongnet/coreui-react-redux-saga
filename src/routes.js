import React from 'react';
import DefaultLayout from './containers/DefaultLayout';

const Dashboard = React.lazy(() => import('./containers/Pages/Dashboard/Dashboard'));
const ReduxSaga = React.lazy(() => import('./containers/Pages/ReduxSaga/ReduxSaga'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home', component: DefaultLayout },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/demo/redux-saga', name: 'ReduxSaga', component: ReduxSaga }
];

export default routes;
